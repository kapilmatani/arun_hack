from dbcommon.models.profile import User
from apps.bookings.models import Booking
#from django.db.models import Q

wrong_email_user_objects = User.objects.exclude(Q(email__contains='@'))
wrong_email_user_objects = [wrong_email_user_objects[0]]
booking_objs = Booking.objects.filter(user_id__in=wrong_email_user_objects)
user_email_map = {}
changed_map = {}
for booking_obj in booking_objs:
    user_email_map[booking_obj.user_id_id] = booking_obj.guest_email

for user_obj in wrong_email_user_objects:
    user_id = user_obj.id
    user_email = user_obj.email
    if user_email_map.get(user_id):
        booking_user_email = user_email_map[user_obj.id]
        if '@' in booking_user_email:
            #user_obj.email = booking_user_email
            #changed_map[user_id] = (user_email, booking_user_email)
            #user_obj.save()
            print ('user_email %s, booking_email: %s' %(user_obj.email, booking_user_email))
