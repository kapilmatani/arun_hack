import json
import csv
import os
import sys
import time
from smtplib import SMTP_SSL as SMTP
from email import encoders
from email.mime.text import MIMEText
import smtplib
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
import shlex
from subprocess import Popen, PIPE
from threading import Timer

cannot_check=0
sensu_notrunning_count=0
sensu_running_count=0
timeout_sec=30


with open('scripts/jsontest2.json', 'r') as f:
    distros_dict = json.load(f)

for distro in distros_dict['Reservations']:
    for instance in distro['Instances']:
        if 'InstanceType' in instance:
            instancetype=instance['InstanceType']
        if 'State' in instance:
            state=instance['State']['Name']
            if state=='terminated':
                ipaddress=''
                instancetype=''
                tag=''
                pod=''
            else:
                if 'PrivateIpAddress' in instance:
                    ipaddress=instance['PrivateIpAddress']
                if 'Tags' in instance:
                    tags = instance['Tags']
                    tag = [tag for tag in tags if tag['Key']=='Name'][0]['Value']
                    if "datascience-p-demandforecastv03-01a" not in tag and "devops-p-sensu" not in tag and "emr" not in tag and "vpn-gateway" not in tag and "blog" not in tag and "jenkins-master" not in tag:
                       cmd1='ssh -i /var/lib/jenkins/.ssh/conman_id_rsa conman@{ipaddress} "service sensu-client status |grep running"'.format(ipaddress=ipaddress)
                       proc = Popen(shlex.split(cmd1), stdout=PIPE, stderr=PIPE)
                       timer = Timer(timeout_sec, proc.kill)
                       try:
                           timer.start()
                           output,err = proc.communicate()
                       finally:
                           timer.cancel()
                       output=output.decode("utf-8")
                       if "running" not in output:
                            sensu_notrunning_count=sensu_notrunning_count+1
                            cmd4='ssh -i /var/lib/jenkins/.ssh/conman_id_rsa conman@{ipaddress} "sudo service sensu-client start"'.format(ipaddress=ipaddress)
                            proc4 = Popen(shlex.split(cmd4), stdout=PIPE, stderr=PIPE)
                            print(cmd4)
                       else:
                            sensu_running_count=sensu_running_count+1
                            cmd2='ssh -i /var/lib/jenkins/.ssh/conman_id_rsa conman@{ipaddress} "sudo chown root:devops-nonsudo /var/run/docker.sock"'.format(ipaddress=ipaddress)
                            proc2 = Popen(shlex.split(cmd2), stdout=PIPE, stderr=PIPE)
                            cmd3='ssh -i /var/lib/jenkins/.ssh/conman_id_rsa conman@{ipaddress} "sudo chmod 644 /usr/local/bin/scripts/logrotate-treebo.conf"'.format(ipaddress=ipaddress)
                            proc3 = Popen(shlex.split(cmd3), stdout=PIPE, stderr=PIPE)
                            cmd5='ssh -i /var/lib/jenkins/.ssh/conman_id_rsa conman@{ipaddress} "echo "Y" |sudo apt-get remove ntop"'.format(ipaddress=ipaddress)
                            proc5 = Popen(shlex.split(cmd5), stdout=PIPE, stderr=PIPE)
                    else:
                       cannot_check=cannot_check+1

                       
print(sensu_running_count,"sensu running count")
print(sensu_notrunning_count,"sensu not running count")
print(cannot_check,"cannot check count")
total=cannot_check+sensu_notrunning_count+sensu_running_count
print(total)

