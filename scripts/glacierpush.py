import boto3, os, time, csv

glacier_client = boto3.client('glacier','ap-south-1')
s3_client = boto3.client('s3','ap-south-1')

try:
    download_dir = "archiveId_vaultName_mapping_prod.csv"
    file1 = open(download_dir, "a")
    for file in os.listdir():
        #print(file)
        if file.endswith(".gz"):
            print(file[:-7])
            vault_name_with_date_dbname = file[:-7]
            print(vault_name_with_date_dbname)
            vault_name_with_dbname = file[11:-7]
            print(vault_name_with_dbname)
            breaker = vault_name_with_dbname.find('-')
            vault_name = vault_name_with_dbname[breaker+1:]
            print(vault_name)
            db_name = vault_name_with_dbname[:breaker]
            print(db_name)
            date = vault_name_with_date_dbname[:10]
            print(date)
            vault_create_response = glacier_client.create_vault(
                vaultName=vault_name
            )
            print(vault_create_response)

            # s3 push
            file_to_upload = open(file, mode="rb")
            bucket_name = 'treebo-dbdump'
            directory_db = db_name  # it's name of your folders
            put_obj_db_response = s3_client.put_object(Bucket=bucket_name, Key=(directory_db + '/'))
            print(put_obj_db_response)
            directory_table = db_name + '/' + vault_name
            put_obj_table_response = s3_client.put_object(Bucket=bucket_name, Key=(directory_table + '/'))
            print(put_obj_table_response)
            directory_date = db_name + '/' + vault_name + '/' + date
            put_obj_date_response = s3_client.put_object(Bucket=bucket_name, Key=(directory_date + '/'))
            print(put_obj_date_response)
            file_upload_s3_response = s3_client.upload_file(file, bucket_name, directory_date+'/{}'.format(file))
            print(file_upload_s3_response)


            # glacier push
            time.sleep(5)
            upload_archive_response = glacier_client.upload_archive(vaultName=vault_name, archiveDescription='db dump for table' + vault_name, body=file_to_upload)
            print(upload_archive_response)
            print(upload_archive_response['archiveId'])
            archiveId = upload_archive_response['archiveId']

            vault_name_with_date = date + '-' + vault_name
            write_row = str(vault_name_with_date) + "," + str(archiveId) + "\n\n"
            print(write_row)
            file1.write(write_row)

except Exception as e:
        print('error is {} not found'.format(e))