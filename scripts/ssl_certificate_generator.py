import boto3, time, paramiko

acm_mumbai_client = boto3.client('acm','ap-south-1')
acm_singapore_client = boto3.client('acm','ap-southeast-1')
acm_nvirginia_client = boto3.client('acm','us-east-1')
ec2_client = boto3.client('ec2','ap-southeast-1')

try:

    #cybertron machine login
    ec2_response = ec2_client.describe_instances(Filters=[
        {
            'Name': 'tag:app',
            'Values': [
                'cybertron-vortex-spot-ec2,cybertron-scavenger-spot-ec2',
            ]
        },
    ],)
    host = ec2_response['Reservations'][0]['Instances'][0]['PrivateIpAddress']
    print(host)

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, username='conman', key_filename='/var/lib/jenkins/.ssh/conman_id_rsa')
    print("Logged in into cybertron server")
    sftp = ssh.open_sftp()
    stdin, stdout, stderr = ssh.exec_command("sudo -S -p '' chmod 755 /home/conman/cert.pem")
    with sftp.open("/home/conman/cert.pem", 'r') as f:
        cert = f.read()

    stdin, stdout, stderr = ssh.exec_command("sudo -S -p '' chmod 755 /home/conman/privkey.pem")
    with sftp.open('/home/conman/privkey.pem', 'r') as f:
        privkey = f.read()
    stdin, stdout, stderr = ssh.exec_command("sudo -S -p '' chmod 755 /home/conman/fullchain.pem")
    with sftp.open('/home/conman/fullchain.pem', 'r') as f:
        fullchain = f.read()
    stdin, stdout, stderr = ssh.exec_command("sudo -S -p '' chmod 755 /home/conman/chain.pem")
    with sftp.open('/home/conman/chain.pem', 'r') as f:
        chain = f.read()

    stdin, stdout, stderr = ssh.exec_command("sudo -S -p '' chmod 644 /home/conman/cert.pem")
    stdin.write("\n")

    stdin, stdout, stderr = ssh.exec_command("sudo -S -p '' chmod 644 /home/conman/privkey.pem")
    stdin.write("\n")

    stdin, stdout, stderr = ssh.exec_command("sudo -S -p '' chmod 644 /home/conman/fullchain.pem")
    stdin.write("\n")

    stdin, stdout, stderr = ssh.exec_command("sudo -S -p '' chmod 644 /home/conman/chain.pem")
    stdin.write("\n")

    stdin.flush()
    ssh.close()
    print("Connection Closed")

    print("Certificate: ",cert)
    print("Private key: ",privkey)
    print("Full chain: ",fullchain)
    print("Chain: ",chain)

    mumbai = acm_mumbai_client.import_certificate(
        CertificateArn='arn:aws:acm:ap-south-1:605536185498:certificate/f14971cc-859b-4c07-9b87-d220aaf5fb18',
        Certificate=cert,
        PrivateKey=privkey,
        CertificateChain=fullchain
    )
    print("Mumbai Response: ", mumbai)

    singapore = acm_singapore_client.import_certificate(
        CertificateArn='arn:aws:acm:ap-southeast-1:605536185498:certificate/ec710bce-5b90-4389-ab93-ec79fb8f648b',
        Certificate=cert,
        PrivateKey=privkey,
        CertificateChain=fullchain
    )

    print("Singapore Response: ", singapore)
    nviriginia = acm_nvirginia_client.import_certificate(
        CertificateArn='arn:aws:acm:us-east-1:605536185498:certificate/e6d4657f-7aea-4075-b637-41625fe82a61',
        Certificate=cert,
        PrivateKey=privkey,
        CertificateChain=fullchain
    )

    print("Nvirginia Response: ", nviriginia)

    ec2_response = ec2_client.describe_instances(Filters=[
        {
            'Name': 'tag:app',
            'Values': [
                'pypi',
            ]
        },
    ],)
    host = ec2_response['Reservations'][0]['Instances'][0]['PrivateIpAddress']
    print(host)

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, username='conman', key_filename='/var/lib/jenkins/.ssh/conman_id_rsa')
    print("Logged in into pypi server")
    sftp = ssh.open_sftp()
    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chmod 777 /etc/nginx/ssl/treebo.be.crt")
    stdin.write("\n")
    stdin.flush()
    f = sftp.open('/etc/nginx/ssl/treebo.be.crt','w')
    f.write(cert)
    f.close()
    print("Created crt file")
    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chown root:root /etc/nginx/ssl/treebo.be.crt")

    stdin.flush()
    time.sleep(2)
    print("Changed owner to root for crt file")

    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chmod 755 /etc/nginx/ssl/treebo.be.crt")

    stdin.flush()
    print("Changed permissions on crt file")



    time.sleep(2)
    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chmod 777 /etc/nginx/ssl/treebo.be.key")
    stdin.write("\n")
    stdin.flush()
    time.sleep(2)
    f = sftp.open('/etc/nginx/ssl/treebo.be.key','w')
    f.write(privkey)
    f.close()
    print("Created private key file")
    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chown root:root /etc/nginx/ssl/treebo.be.key")

    stdin.flush()
    time.sleep(2)
    print("Changed owner to root for private key file")

    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chmod 755 /etc/nginx/ssl/treebo.be.key")

    stdin.flush()
    print("Changed permissions on private key file")


    time.sleep(2)
    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chmod 777 /etc/nginx/ssl/ca_bundle.crt")
    stdin.write("\n")
    stdin.flush()
    time.sleep(2)
    f = sftp.open('/etc/nginx/ssl/ca_bundle.crt','w')
    f.write(chain)
    f.close()
    print("Created ca bundle file")
    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chown root:root /etc/nginx/ssl/ca_bundle.crt")

    stdin.flush()
    time.sleep(2)
    print("Changed owner to root for ca bundle file")

    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chmod 755 /etc/nginx/ssl/ca_bundle.crt")

    stdin.flush()
    print("Changed permissions on ca bundle file")

    time.sleep(2)


    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chmod 777 /etc/nginx/ssl/treebo.be-bundle.pem")
    stdin.write("\n")
    stdin.flush()
    time.sleep(2)
    f = sftp.open('/etc/nginx/ssl/treebo.be-bundle.pem','w')
    f.write(fullchain)
    f.close()
    print("Created treebo.be-bundle.pem file")
    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chown root:root /etc/nginx/ssl/treebo.be-bundle.pem")

    stdin.flush()
    time.sleep(2)
    print("Changed owner to root for treebo.be-bundle.pem file")

    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chmod 644 /etc/nginx/ssl/treebo.be-bundle.pem")

    stdin.flush()
    print("Changed permissions on treebo.be-bundle.pem file")
    time.sleep(2)
    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' service nginx restart")
    stdin.write("\n")
    stdin.flush()
    print("Nginx restarted on pypi")
    time.sleep(10)
    ssh.close()
    print("Connection Closed")


    #haproxy update
    ec2_response = ec2_client.describe_instances(Filters=[
        {
            'Name': 'tag:app',
            'Values': [
                'haproxy-cluster-ec2',
            ]
        },
    ],)
    host = ec2_response['Reservations'][0]['Instances'][0]['PrivateIpAddress']
    print(host)

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, username='conman', key_filename='/var/lib/jenkins/.ssh/conman_id_rsa')
    print("Logged in into haproxy server")
    sftp = ssh.open_sftp()
    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chmod 777 /opt/docker/haproxy/haproxy-cluster/ssl/treebo.be.pem")
    stdin.write("\n")
    stdin.flush()
    time.sleep(2)
    print("Pem file permission changed to 777")
    f = sftp.open('/opt/docker/haproxy/haproxy-cluster/ssl/treebo.be.pem', 'w')
    f.write(privkey)
    f.write(fullchain)
    f.close()
    print("Created ssl file")
    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chown root:root /opt/docker/haproxy/haproxy-cluster/ssl/treebo.be.pem")

    stdin.flush()
    time.sleep(2)
    print("Changed owner to root for ssl file")

    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' chmod 755 /opt/docker/haproxy/haproxy-cluster/ssl/treebo.be.pem")

    stdin.flush()
    print("Changed permissions on ssl file")
    time.sleep(2)
    stdin, stdout, stderr = ssh.exec_command(
        "sudo -S -p '' docker kill -s HUP haproxy-cluster")
    stdin.write("\n")
    stdin.flush()
    print("haproxy Docker killed")
    time.sleep(10)
    ssh.close()
    print("Connection Closed")

except Exception as e:
        print('error is: {}'.format(e))