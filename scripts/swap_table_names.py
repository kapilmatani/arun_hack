import sys, psycopg2

try:
    db_username = sys.argv[1]
    db_password = sys.argv[2]
    db_host = sys.argv[3]
    db_name = sys.argv[4]
    table_name = sys.argv[5]

    myConnection = psycopg2.connect(host=db_host, user=db_username, dbname=db_name, port='5432', password=db_password)

    cur = myConnection.cursor()
    cur.execute("END;")
    new_table_name = table_name + '_new'
    print(new_table_name)
    create_new_table = """CREATE TABLE public.{} (LIKE public.{} INCLUDING ALL);""".format(new_table_name,table_name)
    cur.execute(create_new_table)

    alter_owner = """ALTER TABLE public.{} OWNER TO {};""".format(new_table_name,db_username)
    cur.execute(alter_owner)

    alter_inherit = """ALTER TABLE public.{} INHERIT public.{};""".format(table_name,new_table_name)
    cur.execute(alter_inherit)

    begin = """BEGIN;"""
    cur.execute(begin)
    lock_table_old = """LOCK TABLE public.{} IN ACCESS EXCLUSIVE MODE;""".format(table_name)
    cur.execute(lock_table_old)

    lock_table_new ="""LOCK TABLE public.{} IN ACCESS EXCLUSIVE MODE;""".format(new_table_name)
    cur.execute(lock_table_new)

    old_rename = table_name + '_old'
    alter_rename_old = """ALTER TABLE public.{} RENAME TO {};""".format(table_name,old_rename)
    cur.execute(alter_rename_old)

    alter_rename_new = """ALTER TABLE public.{} RENAME TO {};""".format(new_table_name,table_name)
    cur.execute(alter_rename_new)

    commit = """COMMIT;"""
    cur.execute(commit)

    #move_data = """insert into public.{} select * from public.{} where modified > '2020-05-27'""".format(new_table_name,table_name)
    #cur.execute(move_data)

    myConnection.commit()

except Exception as e:
        print('{}'.format(e))
        myConnection.rollback()

finally:
    #closing database connection.
    if(myConnection):
        cur.close()
        myConnection.close()
        print("PostgreSQL connection is closed")
