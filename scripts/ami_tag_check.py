import boto3

ec2_client = boto3.client('ec2',region_name = 'ap-south-1')
ami_describe = ec2_client.describe_images(Owners=['self'])
#print('Describing AMI {}'.format(ami_describe))


for ami in ami_describe['Images']:
    delete_after_check = False
    delete_after_value = ""
    ami_name = ""
    #print('AMI Describe: {}'.format(ami))
    ami_id = ami['ImageId']
    #print('AMI Id: {}'.format(ami_id))
    try:
        ami_tags = ami['Tags']
        #print('AMI Tags: {}'.format(ami_tags))
        for ami_tag in ami_tags:
            #print("AMI Tag: {}".format(ami_tag))
            ami_key = ami_tag['Key']
            #print(ami_key)
            if(ami_key == 'DeleteAfter'):
                delete_after_check = True
                delete_after_value = ami_tag['Value']
            if(ami_key == 'Name'):
                ami_name = ami_tag['Value']
            #        ami_value = ami_tag['Value']
            #        print("AMI Value: {}".format(ami_value))
        if(delete_after_check == True):
            print(ami_name + " " + delete_after_value)
    except:
        continue
