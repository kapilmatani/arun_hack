import boto3

region = 'ap-southeast-1'
instances = ['i-083246461791af466']
ec2 = boto3.client('ec2', region_name=region)
ec2.stop_instances(InstanceIds=instances)
print('stopped your instances: ' + str(instances))