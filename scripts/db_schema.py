import sys, random, string, boto3, psycopg2, paramiko, time,json
#to run: python3 create_db_schema.py staging service_name(as per secret manager) tenant_id ldap_username ldap_password
try:
    client = boto3.client('secretsmanager')
    client_ec2 = boto3.client('ec2')
    environment = sys.argv[1] #staging or production
    service_name = sys.argv[2]
    tenant = sys.argv[3]
    #username = sys.argv[4]
    #password = sys.argv[5]
    secretId = 'treebo/'+environment+'/apps/'+service_name+'/postgres'
    print(secretId)
    response = client.get_secret_value(
        SecretId=secretId
    )
    secretString = json.loads(response['SecretString'])
    print(secretString)
    db_username = secretString['username']
    #db_username ='web_loyalty'
    print(db_username)
    db_password = secretString['password']
    #db_password='f1L5*5Li83b6'
    print(db_password)
    db_host = secretString['host']
    #db_host = 'direct-p-loyalty-master-new.cwbpdp5vvep9.ap-south-1.rds.amazonaws.com'
    print(db_host)
    db_port= secretString['port']
    #db_port = '5432'
    print(db_port)
    db_name = secretString['dbname']
    #db_name ='staff_management'
    print(db_name)
    letters = string.ascii_letters
    user_password = ''.join(random.choice(letters) for i in range(15))
    print(user_password)
    user_name = tenant + "_user_" + service_name
    print(user_name)
    tenant_admin_password = ''.join(random.choice(letters) for i in range(15))
    print("tenant_Admin_password: {}".format(tenant_admin_password))
    myConnection = psycopg2.connect(host=db_host, user=db_username, dbname=db_name, port=db_port, password=db_password)
    cur = myConnection.cursor()
    cur.execute("END;")
    # create_schema_extensions = """create schema extensions;"""
    # cur.execute(create_schema_extensions)
    # grant_usage_extension = """grant usage on schema extensions to public;"""
    # cur.execute(grant_usage_extension)
    #
    # grant_execute = """grant execute on all functions in schema extensions to public;"""
    # cur.execute(grant_execute)
    #
    # alter_default_execute = """alter default privileges in schema extensions grant execute on functions to public;"""
    # cur.execute(alter_default_execute)
    #
    # alter_default_usage = """alter default privileges in schema extensions grant usage on types to public;"""
    # cur.execute(alter_default_usage)
    # alter_role = """ALTER ROLE {} IN DATABASE {} SET search_path TO "$user",public;""".format(
    #     db_username,db_name)
    # cur.execute(alter_role)
    #create_ten_admin = """create user tenant_admin with password '{}';""".format(tenant_admin_password)
    # #create_ten_admin = """create user tenant_admin with password '{}';""".format()
    #cur.execute(create_ten_admin)
    #
    #grant_admin = """GRANT tenant_admin TO {};""".format(db_username)
    #cur.execute(grant_admin)
    create_user = """CREATE USER {} WITH PASSWORD '{}';""".format(user_name,user_password)
    #create_user = """CREATE USER {} WITH PASSWORD '{}';""".format(user_name, 'hcqVDZVbLdwgcDu')
    cur.execute(create_user)
    grant_user = """GRANT {} TO tenant_admin""".format(user_name)
    cur.execute(grant_user)
    grant_connect ="""GRANT CONNECT ON DATABASE {} TO {}""".format(db_name,user_name)
    cur.execute(grant_connect)
    new_schema = tenant + "_schema"
    create_schema = """CREATE SCHEMA {} AUTHORIZATION tenant_admin""".format(new_schema)
    cur.execute(create_schema)
    set_search_path = """ALTER ROLE {} IN DATABASE {} SET search_path = {}""".format(user_name,db_name,new_schema)
    cur.execute(set_search_path)
    grant_usage = """GRANT USAGE,CREATE ON SCHEMA {} TO {}""".format(new_schema,user_name)
    cur.execute(grant_usage)
    grant_usage = """GRANT USAGE ON SCHEMA {} TO {}""".format(new_schema,user_name)
    cur.execute(grant_usage)
    grant_create = """GRANT CREATE ON SCHEMA {} TO tenant_admin""".format(new_schema)
    cur.execute(grant_create)
    alter_privileges_tables = """ALTER DEFAULT PRIVILEGES FOR ROLE tenant_admin GRANT ALL ON TABLES TO {}""".format(user_name)
    cur.execute(alter_privileges_tables)
    alter_privileges_sequences = """ALTER DEFAULT PRIVILEGES FOR ROLE tenant_admin GRANT ALL ON SEQUENCES TO {}""".format(user_name)
    cur.execute(alter_privileges_sequences)
    myConnection.commit()
    # Upload credentials of new db to secret manager
    new_secret_id = tenant + '/' +environment +'/apps/' + service_name + '/postgres'
    new_secret = {"username": user_name, "password":user_password, "host":db_host, "port": db_port, "dbname": db_name}
    response = client.create_secret(
        Name=new_secret_id,
        SecretString = json.dumps(new_secret)
    )
    print(response)
    #edit pgbouncer config
    # ec2_response = client_ec2.describe_instances(Filters=[
    #     {
    #         'Name': 'tag:app',
    #         'Values': [
    #             'pgbouncer-ec2',
    #         ]
    #     },
    # ],)
    # host = ec2_response['Reservations'][0]['Instances'][0]['PrivateIpAddress']
    # print(host)
    #
    # ssh = paramiko.SSHClient()
    # ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # ssh.connect(host, username=username, password=password)
    # sftp = ssh.open_sftp()
    # stdin, stdout, stderr = ssh.exec_command(
    #     "sudo -S -p '' chmod 777 /etc/pgbouncer/userlist.txt")
    # stdin.write(password + "\n")
    # stdin.flush()
    # time.sleep(10)
    #
    # f = sftp.open('/etc/pgbouncer/userlist.txt','a+')
    #
    # new_line = '"' + user_name + '" ' + '"' + user_password + '"' + '\n'
    # f.write(new_line)
    # f.close()
    #
    # stdin, stdout, stderr = ssh.exec_command(
    #     "sudo -S -p '' chmod 640 /etc/pgbouncer/userlist.txt")
    # stdin.write(password + "\n")
    # stdin.flush()
    # time.sleep(10)
    # stdin, stdout, stderr = ssh.exec_command(
    #     "sudo -S -p '' service pgbouncer reload")
    # stdin.write(password + "\n")
    # stdin.flush()
    # time.sleep(10)
    # ssh.close()
except Exception as e:
        print('{}'.format(e))
        myConnection.rollback()
finally:
    #closing database connection.
    if(myConnection):
        cur.close()
        myConnection.close()
        print("PostgreSQL connection is closed")