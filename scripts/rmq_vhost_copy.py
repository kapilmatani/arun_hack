import sys, requests, random, string

try:
    old_vhost = sys.argv[1]
    tenant = sys.argv[2]
    url = 'http://shared-rmq.treebo.com:15672/api/'
    headers = {'Content-type': 'application/json'}

    #add same exchanges as on old vhost to new vhost
    r = requests.get(url + 'exchanges/'+ old_vhost, auth=('devops', 'M3800'))
    exchanges = r.json()
    new_vhost = old_vhost + '_' + tenant
    print(new_vhost)
    vh = requests.put(url + 'vhosts/'+ new_vhost,  auth=('devops', 'M3800'))
    print(vh)
    for exchange in exchanges:
        print(exchange)
        arguements = exchange['arguments']
        print(arguements)
        auto_delete = exchange['auto_delete']
        print(auto_delete)
        durable = exchange['durable']
        print(durable)
        internal = exchange['internal']
        print(internal)
        name = exchange['name']
        print(name)
        policy = exchange['policy']
        print(policy)
        type = exchange['type']
        print(type)
        user_who_performed_action = exchange['user_who_performed_action']
        print(user_who_performed_action)
        vhost = new_vhost
        print(vhost)
        data = {'auto_delete': auto_delete,
                'durable': durable, 'internal': internal, 'type': type,
                'user_who_performed_action': user_who_performed_action, 'arguments': arguements}
        print(data)
        vh_exchange = requests.put(url + 'exchanges/' +vhost + '/' + name, auth=('devops', 'M3800'), headers=headers,
                                    json=data)
        print(vh_exchange)



    #add same queues from old vhost to new vhost
    queue_get = requests.get(url + 'queues/' + old_vhost, auth=('devops', 'M3800'))
    queues = queue_get.json()
    for queue in queues:
        print(queue)
        print("\n")
        queue_arguements = queue['arguments']
        queue_autodelete = queue['auto_delete']
        queue_durable = queue['durable']
        queue_effective_policy_definition = queue['effective_policy_definition']
        queue_name = queue['name']
        queue_node = queue['node']
        queue_data = {"arguments":queue_arguements, "auto_delete":queue_autodelete, "durable": queue_durable, "effective_policy_definition":queue_effective_policy_definition,
                      "node":queue_node}
        queue_attach = requests.put(url + 'queues/' + new_vhost + '/' + queue_name, auth=('devops', 'M3800'), headers=headers,
                                    json = queue_data)
        print(queue_attach)



    #attach same policies from older to new vhost
    policy_get = requests.get(url + 'policies/' + old_vhost , auth=('devops', 'M3800'))
    policies = policy_get.json()
    print(policies)
    for policy in policies:
        policy_vhost = policy['vhost']
        print(policy_vhost)
        policy_name = policy['name']
        print(policy_name)
        policy_pattern = policy['pattern']
        print(policy_pattern)
        policy_applyto = policy['apply-to']
        print(policy_applyto)
        policy_defination = policy['definition']
        print(policy_defination)
        policy_priority = policy['priority']
        print(policy_priority)
        policy_data = {"pattern":policy_pattern, "definition": policy_defination, "priority":policy_priority, "apply-to": policy_applyto}
        policy_attach = requests.put(url + 'policies/' + new_vhost + '/' + policy_name, auth=('devops', 'M3800'),
                                  headers=headers,
                                  json=policy_data)
        print(policy_attach)

    #shovel-management
    shovel_get = requests.get(url +'shovels' , auth=('devops', 'M3800'))
    shovels = shovel_get.json()
    print(shovels)
    count = 0
    for shovel in shovels:
        print(shovel)
        state = shovel['state']
        print(state)
        if state == 'running':
            source_uri = shovel['src_uri']
            source = source_uri.split('/')
            source_vhost = source[-1]
            source_vhost = source_vhost + '_' + tenant
            print(source_vhost)
            dest_uri = shovel['dest_uri']
            dest = dest_uri.split('/')
            dest_vhost = dest[-1]
            dest_vhost = dest_vhost + '_' + tenant
            print(dest_vhost)
            source_vhost_check = requests.get(url + 'definitions/' + source_vhost, auth=('devops', 'M3800'))
            #print(source_vhost_check.json())
            dest_vhost_check = requests.get(url + 'definitions/' + dest_vhost, auth=('devops', 'M3800'))
            #print(dest_vhost_check.json())

            source_vhost_check = source_vhost_check.json()
            dest_vhost_check = dest_vhost_check.json()
            if 'error' not in source_vhost_check and 'error'  not in dest_vhost_check:
                shovel_name = shovel['name']
                shovel_vhost = shovel['vhost'] + '_' + tenant
                print(shovel_vhost)
                shovel_type = shovel['type']
                src_uri = 'amqp://devops:M3800@shared-rmq.treebo.com:5672/' + source_vhost
                #for elements in source[2:-1]:
                #    src_uri = src_uri + elements + '/'
                #src_uri = src_uri + source_vhost
                print (src_uri)
                dest_uri = 'amqp://devops:M3800@shared-rmq.treebo.com:5672/' + dest_vhost
                # for elements in dest[2:-1]:
                #     dest_uri = dest_uri + elements + '/'
                # dest_uri = dest_uri + dest_vhost
                print(dest_uri)
                shovel_src_protocol = shovel['src_protocol']
                shovel_dest_protocol = shovel['dest_protocol']
                
                
                if 'src_exchange_key' in shovel:
                    shovel_src_exchange_key = shovel['src_exchange_key']
                if 'dest_exchange_key' in shovel:
                    shovel_dest_exchange_key = shovel['dest_exchange_key']
                if 'shovel_src_exchange' in shovel:
                    shovel_src_exchange = shovel['src_exchange']
                if 'shovel_dest_exchange' in shovel:
                    shovel_dest_exchange = shovel['dest_exchange']

                if 'src_exchange_key' in shovel and 'dest_exchange_key' in shovel and 'shovel_src_exchange' in shovel:
                    shovel_data = {"value": {'src-uri': src_uri, 'src-protocol': shovel_src_protocol,
                               'dest-protocol': shovel_dest_protocol, 'dest-uri': dest_uri,
                                'src-exchange': shovel_src_exchange,
                                'dest-exchange': shovel_dest_exchange, 'src-exchange-key': shovel_src_exchange_key, 'dest-exchange-key': shovel_dest_exchange_key}}

                elif 'src_exchange_key' in shovel and 'dest_exchange_key' not in shovel and 'shovel_src_exchange' in shovel:
                    shovel_data = {"value": {'src-uri': src_uri, 'src-protocol': shovel_src_protocol,
                                             'dest-protocol': shovel_dest_protocol, 'dest-uri': dest_uri,
                                             'src-exchange': shovel_src_exchange,
                                             'dest-exchange': shovel_dest_exchange,
                                             'src-exchange-key': shovel_src_exchange_key}}
                elif 'src_exchange_key' not in shovel and 'dest_exchange_key' in shovel and 'shovel_src_exchange' in shovel:
                    shovel_data = {"value": {'src-uri': src_uri, 'src-protocol': shovel_src_protocol,
                                             'dest-protocol': shovel_dest_protocol, 'dest-uri': dest_uri,
                                             'src-exchange': shovel_src_exchange,
                                             'dest-exchange': shovel_dest_exchange,
                                             'dest-exchange-key': shovel_dest_exchange_key}}
                                             
                elif 'src_exchange_key' in shovel and 'dest_exchange_key' in shovel and 'shovel_src_exchange' not in shovel:
                    shovel_data = {"value": {'src-uri': src_uri, 'src-protocol': shovel_src_protocol,
                               'dest-protocol': shovel_dest_protocol, 'dest-uri': dest_uri, 'src-exchange-key': shovel_src_exchange_key, 'dest-exchange-key': shovel_dest_exchange_key}}

                elif 'src_exchange_key' in shovel and 'dest_exchange_key' not in shovel and 'shovel_src_exchange' not in shovel:
                    shovel_data = {"value": {'src-uri': src_uri, 'src-protocol': shovel_src_protocol,
                                             'dest-protocol': shovel_dest_protocol, 'dest-uri': dest_uri,
                                             'src-exchange-key': shovel_src_exchange_key}}
                elif 'src_exchange_key' not in shovel and 'dest_exchange_key' in shovel and 'shovel_src_exchange' not in shovel:
                    shovel_data = {"value": {'src-uri': src_uri, 'src-protocol': shovel_src_protocol,
                                             'dest-protocol': shovel_dest_protocol, 'dest-uri': dest_uri,
                                             'dest-exchange-key': shovel_dest_exchange_key}}
                                             
                elif 'src_exchange_key' not in shovel and 'dest_exchange_key' not in shovel and 'shovel_src_exchange' not in shovel:
                    shovel_data = {"value": {'src-uri': src_uri, 'src-protocol': shovel_src_protocol,
                                             'dest-protocol': shovel_dest_protocol, 'dest-uri': dest_uri,
                                             }}
                else:
                    shovel_data = {"value": {'src-uri': src_uri, 'src-protocol': shovel_src_protocol,
                                             'dest-protocol': shovel_dest_protocol, 'dest-uri': dest_uri,
                                             'src-exchange': shovel_src_exchange,
                                             'dest-exchange': shovel_dest_exchange}}
                print(shovel_data)

                shovel_create = requests.put(url + 'parameters/shovel/' + shovel_vhost + '/' + shovel_name, auth=('devops', 'M3800'), headers=headers,
                                  json=shovel_data)
                print(shovel_create)
                
    #create user for the vhost
    letters = string.ascii_lowercase
    password = ''.join(random.choice(letters) for i in range(10))

    user_data = {"password":password,"tags":"administrator"}
    print(user_data)
    add_user = requests.put(url + 'users/' + new_vhost, auth=('devops', 'M3800'),headers=headers,
                                 json=user_data)
    print(add_user)

    #give above user vhost permission
    permission_data = {"configure":".*","write":".*","read":".*"}
    user_permission = requests.put(url + 'permissions/' + new_vhost + '/' + new_vhost, auth=('devops', 'M3800'),headers=headers,
                                 json=permission_data)
    print(user_permission)

    user_get = requests.get(url + 'users/' + new_vhost + '/' + 'permissions', auth=('devops', 'M3800'))
    print(user_get.json())
    
except Exception as e:
        print('error is {} not found'.format(e))