import json
import csv
import os
import sys
import time
from smtplib import SMTP_SSL as SMTP
from email import encoders
from email.mime.text import MIMEText
import smtplib
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart

fixb2bcount=9
fixrmscount=16
fixdirectcount=18
fixcrscount=5
fixdataplatformcount=7
fixdatasciencecount=8
fixdevopscount=16
fixsharedcount=0

if os.path.exists("prodmachines.csv"):
  os.remove("prodmachines.csv")
else:
  print('File does not exists')

with open('scripts/jsontest1.json', 'r') as f:
    distros_dict = json.load(f)

totalcount=0
b2bcount=0
crscount=0
rmscount=0
directcount=0
dataplatformcount=0
datasciencecount=0
devopscount=0
sharedcount=0

for distro in distros_dict['Reservations']:
    for instance in distro['Instances']:
        if 'InstanceType' in instance:
            instancetype=instance['InstanceType']
            print(instancetype)
        if 'State' in instance:
            state=instance['State']['Name'] 
            print(state)
            if state=='terminated':
                ipaddress=''
                instancetype=''
                tag=''
                pod=''
            else:
                if 'PrivateIpAddress' in instance:
                    ipaddress=instance['PrivateIpAddress'] 
                if 'Tags' in instance:
                    tags = instance['Tags']
                    tag = [tag for tag in tags if tag['Key']=='Name'][0]['Value']
                    print(tag)
                    pod = [pod for pod in tags if pod['Key']=='pod'][0]['Value']
                    print(pod)
                    if pod == 'b2b':
                        b2bcount=b2bcount+1
                    if pod == 'rms':
                        rmscount=rmscount+1
                    if pod == 'direct':
                        directcount=directcount+1
                    if pod == 'crs':
                        crscount=crscount+1
                    if pod == 'dataplatform':
                        dataplatformcount=dataplatformcount+1
                    if pod == 'datascience':
                        datasciencecount=datasciencecount+1
                    if pod == 'devops':
                        devopscount=devopscount+1
                    if pod == 'shared':
                        sharedcount=sharedcount+1
                    totalcount=totalcount+1
                    with open('prodmachines.csv', 'a') as writeFile:
                      writer = csv.writer(writeFile)
                      fieldnames = ['IPAddress','InstanceType','Name','State','Pod']
                      writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
                      writer.writerow({'IPAddress': ipaddress, 'InstanceType': instancetype, 'Name':tag,'State':state,'Pod': pod})

expectedcount=b2bcount+rmscount+directcount+crscount+dataplatformcount+datasciencecount+devopscount+sharedcount

print(rmscount)
print(b2bcount)
print(directcount)
print(crscount)
print(dataplatformcount)
print(datasciencecount)
print(devopscount)
print(totalcount)
print(expectedcount)
writeFile.close()

def send_email(pod,count,fixcount):
    #for sending the mail
    SMTPserver = 'email-smtp.eu-west-1.amazonaws.com'
    EMAIL_PORT = 587

    sender = 'Prod Scripts <dev@treebohotels.com>'

    destination = ['kaddy@treebohotels.com','arun.midha@treebohotels.com','mayank.khandelwal@treebohotels.com','abhinav.nigam@treebohotels.com','soumya.das@treebohotels.com','ashish.singh@treebohotels.com']
    #destination = ['arun.midha@treebohotels.com']

    USERNAME = "AKIAYZ7GE5SNATIB23OR"
    PASSWORD = "AoC9Wwy92HhTiO3dFtF8B8PuvnA1zKTgf4JgXcwaab9i"

    # typical values for text_subtype are plain, html, xml
    text_subtype = 'plain'
    body="Machine count should be "+ str(fixcount) + " for "+str(pod)+" but current count is "+str(count)
    #body="Fix machine numbers for:" +str(rmscount)+"(RMS),"+str(b2bcount)+"(B2B),"+str(directcount)+"(Direct),"+str(crscount)+"(CRS),"+str(dataplatformcount)+"(DataPlatform),"+str(datasciencecount)+"(DataScience),"+str(devopscount)+"(Devops): "+str(totalcount)+"(Total)"
    #subject="Urgent:AWS Prod machine count"
    subject="Urgent:AWS Production machine Count is not correct for "+str(pod)+" POD"
    try:
        msg = MIMEMultipart()
        #msg = MIMEText(content, text_subtype)
        msg['Subject']=       subject
        msg['From']   = sender # some SMTP servers will do this automatically, not all

        msg.attach(MIMEText(body, 'plain'))
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open("prodmachines.csv", "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="prodmachines.csv"')

        msg.attach(part)
        conn = SMTP(SMTPserver)
        conn.set_debuglevel(False)
        conn.login(USERNAME, PASSWORD)
        try:
            conn.sendmail(sender, destination, msg.as_string())
        finally:
            conn.quit()

    except getopt.GetoptError as e:
        sys.exit( "mail failed; %s" % str(exc) )


if rmscount==fixrmscount:
    print("RMS Production count is correct and count is " +str(fixrmscount))
else:
    print('rms',rmscount)
    send_email('RMS',rmscount,fixrmscount)

if b2bcount==fixb2bcount:
    print("B2B Production count is correct and count is " +str(fixb2bcount))
else:
    print('B2B',b2bcount)
    send_email('B2B',b2bcount,fixb2bcount)

if crscount==fixcrscount:
    print("CRS Production count is correct and count is "+str(fixcrscount))
else:
    print('CRS',crscount)
    send_email('CRS',crscount,fixcrscount)

if directcount==fixdirectcount:
    print("Direct Production count is correct and count is "+str(fixdirectcount))
else:
    print('Direct',directcount)
    send_email('Direct',directcount,fixdirectcount)

if dataplatformcount==fixdataplatformcount:
    print("Dataplatform Production count is correct and count is "+ str(fixdataplatformcount))
else:
    print('DataPlatform',dataplatformcount)
    send_email('DataPlatform',dataplatformcount,fixdataplatformcount)

if datasciencecount==fixdatasciencecount:
    print("DataScience Production count is correct and count is "+ str(fixdatasciencecount))
else:
    print('DataScience',datasciencecount)
    send_email('DataScience',datasciencecount,fixdatasciencecount)

if devopscount==fixdevopscount:
    print("Devops Production count is correct and count is "+ str(fixdevopscount))
else:
    print('Devops',devopscount)
    send_email('Devops',devopscount,fixdevopscount)

if expectedcount==totalcount:
     print("Total Staging count is correct and count is "+ str(expectedcount))
else:
    print('Total',expectedcount)
    send_email('Total',expectedcount,totalcount)
