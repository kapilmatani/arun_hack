import boto3
import time
import requests
import json

webhook_url = "https://hooks.slack.com/services/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"

client = boto3.client('elb')
endtime=time.time()+900.0 #15minutes
count_ha_cluster = 0
count_pgbouncer = 0
count_haproxy = 0

while (time.time()<endtime):
    response = client.describe_load_balancers(LoadBalancerNames=['shared-p-ha-cluster-int-elb'])
    if(len(response['LoadBalancerDescriptions'][0]['Instances']) < 2):
        count_ha_cluster = count_ha_cluster+1

    response = client.describe_load_balancers(LoadBalancerNames=['shared-p-pgbouncer-elb'])
    if(len(response['LoadBalancerDescriptions'][0]['Instances']) < 2):
        count_pgbouncer = count_pgbouncer+1
        
    response = client.describe_load_balancers(LoadBalancerNames=['shared-p-haproxy-cluster-elb'])
    if(len(response['LoadBalancerDescriptions'][0]['Instances']) < 2):
        count_haproxy = count_haproxy+1
        
    time.sleep(300) #5minutes

if(count_ha_cluster>1):
    payload = str({"text": "Alert: Check ELB shared-p-ha-cluster-int-elb, all instances are not present"})
    requests.post(webhook_url,payload)
    
if(count_pgbouncer>1):
    payload = str({"text": "Alert: Check ELB shared-p-pgbouncer-elb, all instances are not present"})
    requests.post(webhook_url,payload)

if(count_haproxy>1):
    payload = str({"text": "Alert: Check shared-p-haproxy-cluster-elb, all instances are not present"})
    requests.post(webhook_url,payload)