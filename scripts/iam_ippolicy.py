import boto3
from collections import Counter

all_users = []
exclude_list = ['all-pod-p-elb-logs','amibackup,b2b-mint','b2b-mint-preprod','b2b-p-corp,b2b-p-epos','b2b-p-mercury','b2b-p-mint','b2b-p-reseller-s3','b2b-p-s3','b2b-s-corp','b2b-s-epos','b2b-s-marvin-s3','b2b-s-mercury','b2b-s-mint','b2b-s-reseller-s3','cloudhealth','cloudops-user','cloudscan','cloudwatch-stats-user','cm-s3-prowl','conman','crs-p-bb-ses','crs-p-cybertron-junkyard-s3','crs-p-cybertron-vortex-s3','crs-p-s3','crs-s-cybertron-vortex-s3','crs-s3-bumblebee-hms','data-science,data-xls-crawling','devops-all-one-click-infra-all','devops-automation','devops-d-devspinup','devops-p-elk-snapshots-s3','devops-p-haproxyip-s3','devops-p-jenkins','devops-p-metabase','devops-p-rmq-ec2','devops-p-sensu','devops-p-sensu-ses','devops-p-serverless','devops-p-uptime-s3','devops-s-elk-elasticache','devops-s-elk-snapshots-s3','devops-s-rmq-msgbackup-s3','devops-s-serverless','devops-sensu','devops-services-limited','direct-s-imgix-cdn','e-pos,elk-snapshot','emr_user','imgix','KMS-keyUser','kops-user','MemoryMetricsPolicy','mercury','mint','mint-jumbo-staysummary-s3','mint.sqs','platform-serverless-lambda','prompt-cloud','rms-notification-prod','rms-notification-stage','rms-p-catalog-s3','rms-s-dynamodb-rackrate','s3employee-upload','s3logs-pushuser','S3Uploader','sensu_ses_lambda','sensu-grafana-read-only-user','serverless-crs-accounts-prod','serverless-crs-accounts-stage','serverless-direct,serverless-direct-prod','serverless-ds-prod','serverless-ds-stage','serverless-rms-prod','serverless-rms-stage','ses-access','ses-smtp-metabase','ses-smtp-platform-user','ses-smtp-user-redash','treebo-b2b-mint-db-backup','treebo-janitor','treebo-mobile-automation','treebo-p-rms,treebo-s3-coupons','treebo-s3-rds-pgdump','treebo-s3-voucher','treebo-ses-ireland-user','treebo-user-admin','vulneye-scan','web-direct-devicefarm','web-p-bugsnag-s3','web-p-imgix-cdn','web-p-s3','web-p-ses','zappa,marvin-s3-treebo-partner-prod-bucket-uploader','platform-serverless-lambda-prod','ses-smtp-user.20200529-175250','ses-smtp-user.20200817-175011','ses-smtp-user.20201214-200100','shared-p-frontend-cybertron-serverless','shared-s-frontend-cybertron-serverless']


client = boto3.client('iam')

response = client.list_users(
    MaxItems=138
)

for i in range(138):
    #print(response['Users'][i]['UserName'])
    all_users.append(response['Users'][i]['UserName'])

final_list = list((Counter(all_users)-Counter(exclude_list)).elements())

print(final_list)

for j in range(46):
    response_pol = client.put_user_policy(
    UserName=final_list[j],
    PolicyName='Source_IPRestrict',
    PolicyDocument='''{
    "Version": "2012-10-17",
    "Statement": {
        "Effect": "Deny",
        "Action": "*",
        "Resource": "*",
        "Condition": {
            "NotIpAddress": {
                "aws:SourceIp": [
                    "35.154.64.73/32",
                    "13.127.245.69/32",
                    "13.251.134.172/32",
                    "35.154.94.242/32",
                    "52.76.27.48"
                ]
            },
            "Bool": {
                "aws:ViaAWSService": "false"
            }
        }
    }
    }''')
