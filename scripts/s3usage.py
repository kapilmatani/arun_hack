import os
import boto3
s3 = boto3.resource('s3')
os.system("rm s3_usage.txt")
for bucket in s3.buckets.all():
    print (bucket.name)
    cmd = """aws s3api list-object-versions --bucket """ + bucket.name + """ | jq -r '.Versions[] | "\(.Key)\t \(.Size/1024/1024) MB"' | sort -k2 -r -n | awk 'NR==1{print $0}''NR==2{print $0}''NR==3{print $0}''BEGIN {total=0}{total+=$2}END{print total/1024" GB"}' >> s3_usage.txt | echo '\n' >> s3_usage.txt """
    os.system(cmd)