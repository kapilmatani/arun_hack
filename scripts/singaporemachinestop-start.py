import json
import csv
import os
import sys
import time
from smtplib import SMTP_SSL as SMTP
from email import encoders
from email.mime.text import MIMEText
import smtplib
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart

with open('scripts/jsontest2.json', 'r') as f:
    distros_dict = json.load(f)

for distro in distros_dict['Reservations']:
    for instance in distro['Instances']:
        if 'InstanceType' in instance:
            instancetype=instance['InstanceType']
        if 'State' in instance:
            state=instance['State']['Name']
            if state!='terminated':
                if 'PrivateIpAddress' in instance:
                    ipaddress=instance['PrivateIpAddress']
                if 'Tags' in instance:
                    tags = instance['Tags']
                    tag = [tag for tag in tags if tag['Key']=='Name'][0]['Value']
                    print(tag)