import boto3, sys, csv

glacier_client = boto3.client('glacier')


try:
    table_name = sys.argv[1]
    date = sys.argv[2]
    print(table_name)
    print(date)
    vault_name_with_date = date + '_' + table_name
    print(vault_name_with_date)
    vault_name = table_name
    describe_vault_response = glacier_client.describe_vault(vaultName=vault_name)
    print(describe_vault_response)
    with open('archiveId_vaultName_mapping.csv', 'r') as csvFile:
        reader = csv.reader(csvFile)
        for row in reader:
            if vault_name_with_date in str(row):
                #row1 = str(row).split(",")
                #print(row1)
                archiveId = row[1]
                #vaultName = vault_name
                print(archiveId)
                #print(vaultName)
                retrieve_archive_response = glacier_client.initiate_job(
                    accountId='-',
                    jobParameters={
                        'Description': 'Archive Retrieval for' + vault_name,
                        'Type': 'archive-retrieval',
                        'ArchiveId': archiveId
                    },
                    vaultName=vault_name,
                 )
                print(retrieve_archive_response)
                list_job_response = glacier_client.list_jobs(vaultName=vault_name)
                print(list_job_response)

    #delete = glacier_client.delete_vault(vaultName=vault_name)
    #print(delete)


except Exception as e:
        print('error is {} not found'.format(e))