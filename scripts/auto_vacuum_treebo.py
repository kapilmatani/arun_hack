import psycopg2
import logging
from datetime import datetime
from datetime import timedelta
import requests
# seting up logger
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
def setup_logger(name,log_file,level):
    handler = logging.FileHandler(log_file)
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger
info_logger = setup_logger('Info_logger', 'dbTruncate_info.log',logging.INFO)
error_logger = setup_logger('Error_logger', 'dbTruncate_error.log',logging.ERROR)
# Database credentials
host_name='direct-p-master.treebo.com'
db_name='treebo'
db_user='treeboadmin'
db_pass='caTwimEx3'
dp_port='5432'
try:
    connection = psycopg2.connect(host=host_name, user=db_user, dbname=db_name, port=dp_port, password=db_pass)
    cur = connection.cursor()
    print('Connected')
    now = datetime.now()
    day_before_yesterday = now - timedelta(days=7)
    day_before_yesterday="'" + str(day_before_yesterday) + "'"
    postgres_delete_query='''DELETE FROM django_session WHERE expire_date < '''
    postgres_select_query= '''SELECT expire_date FROM django_session WHERE expire_date <'''
    cur.execute(postgres_select_query + day_before_yesterday)
    result = cur.fetchall()
    info_logger.info('DELETING THESE RECORDS: {} records'.format(result))
    cur.execute(postgres_delete_query + day_before_yesterday)
    connection.commit()
    # Running autovaccum
    connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    postgres_auto_vaccum_query = '''VACUUM ANALYZE django_session'''
    connection.commit()
    cur.execute(postgres_auto_vaccum_query)
except (Exception, psycopg2.Error) as error:
    #print(error)
    error_logger.error('Error Occoured: {} '.format(error))
    url = 'https://hooks.slack.com/services/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'
    payload = str({"text": "auto_vacuum_treebo script on Jenkins failed with error : {}. Please check".format(error)})
    result = requests.post(url, payload)

finally:
    if(connection):
        cur.close()
        connection.close()
        print('closed')