import csv
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
import sys
from smtplib import SMTP_SSL as SMTP
from email import Encoders
from email.mime.text import MIMEText
from datetime import datetime

from gevent import os

from DB_Conn import DBConn


class SME:
    cur = DBConn(app_name='web').get_connection()
    guery_to_get_all_booking_with_taxcode_as_once = "select organization_taxcode,organization_name,organization_address,user_id_id,id,adult_count,booking_code,booking_status,cancellation_datetime,channel,checkin_date,checkout_date,child_count,comments,coupon_apply,coupon_code,created_at,discount,external_crs_booking_extra_info,external_crs_booking_id,external_crs_booking_version,group_code,guest_email,guest_mobile,guest_name,hotel_id,is_audit,is_complete,is_traveller,member_discount,mm_promo,modified_at,order_id,payment_amount,payment_mode,pretax_amount,rack_rate,rate_plan,rate_plan_meta,room_config,room_id,tax_amount,total_amount,wallet_applied,wallet_deduction from bookings_booking where organization_taxcode  in (select organization_taxcode from bookings_booking where organization_taxcode !='' and booking_status in ('Reserved','Confirm','Temp Booking Created') group by organization_taxcode  having count(organization_taxcode)=1) and date(created_at) = CURRENT_DATE - 1;"
    cur.execute(guery_to_get_all_booking_with_taxcode_as_once)
    result = cur.fetchall()
    file_exists = os.path.isfile('sme.csv')
    if file_exists:
        os.remove('/var/lib/jenkins/jobs/direct-p-smecsvgenerate/workspace/sme.csv')
        file_exists = False
    for every_booking in result:
        booking_gstin = every_booking[0]
        b2b_query = "select * from b2b_bookingattributes where key='gstin' and value='" + str(booking_gstin) + "'"
        cur1 = DBConn(app_name='b2b').get_connection()
        cur1.execute(b2b_query)
        result1 = cur1.fetchall()
        if not result1:
            try:
                with open('sme.csv', 'a') as csvfile:
                    headers = ['id', 'adult_count', 'booking_code', 'booking_status', 'cancellation_datetime',
                               'channel',
                               'checkin_date', 'checkout_date', 'child_count', 'comments', 'coupon_apply',
                               'coupon_code',
                               'created_at', 'discount', 'external_crs_booking_extra_info', 'external_crs_booking_id',
                               'external_crs_booking_version', 'group_code', 'guest_email', 'guest_mobile',
                               'guest_name',
                               'hotel_id', 'is_audit', 'is_complete', 'is_traveller', 'member_discount', 'mm_promo',
                               'modified_at', 'order_id', 'organization_address', 'organization_name',
                               'organization_taxcode', 'payment_amount', 'payment_mode', 'pretax_amount', 'rack_rate',
                               'rate_plan', 'rate_plan_meta', 'room_config', 'room_id', 'tax_amount', 'total_amount',
                               'wallet_applied', 'wallet_deduction']
                    writer = csv.DictWriter(csvfile, delimiter=',', lineterminator='\n', fieldnames=headers)
                    if not file_exists:
                        writer.writeheader()
                        file_exists = True
                    writer.writerow(
                        {'id': every_booking[4], 'adult_count': every_booking[5], 'booking_code': every_booking[6],
                         'booking_status': every_booking[7], 'cancellation_datetime': every_booking[8],
                         'channel': every_booking[9], 'checkin_date': every_booking[10],
                         'checkout_date': every_booking[11], 'child_count': every_booking[12],
                         'comments': every_booking[13], 'coupon_apply': every_booking[14],
                         'coupon_code': every_booking[15], 'created_at': every_booking[16],
                         'discount': every_booking[17], 'external_crs_booking_extra_info': every_booking[18],
                         'external_crs_booking_id': every_booking[19],
                         'external_crs_booking_version': every_booking[20], 'group_code': every_booking[21],
                         'guest_email': every_booking[22], 'guest_mobile': every_booking[23],
                         'guest_name': every_booking[24], 'hotel_id': every_booking[25],
                         'is_audit': every_booking[26], 'is_complete': every_booking[27],
                         'is_traveller': every_booking[28], 'member_discount': every_booking[29],
                         'mm_promo': every_booking[30], 'modified_at': every_booking[31],
                         'order_id': every_booking[32], 'organization_address': every_booking[2],
                         'organization_name': every_booking[1], 'organization_taxcode': every_booking[0],
                         'payment_amount': every_booking[33], 'payment_mode': every_booking[34],
                         'pretax_amount': every_booking[35], 'rack_rate': every_booking[36],
                         'rate_plan': every_booking[37], 'rate_plan_meta': every_booking[38],
                         'room_config': every_booking[39], 'room_id': every_booking[40],
                         'tax_amount': every_booking[41], 'total_amount': every_booking[42],
                         'wallet_applied': every_booking[43], 'wallet_deduction': every_booking[44]})
            except Exception as e:
                print (e)
            finally:
                csvfile.closed
    # for sending the mail
    SMTPserver = 'email-smtp.eu-west-1.amazonaws.com'
    EMAIL_PORT = 587

    sender = 'Prod Scripts <dev@treebohotels.com>'

    destination = ['gaurav.kinra@treebohotels.com', 'utkarsh.srivastava@treebohotels.com',
                   'rajkarishma.bahuriya@treebohotels.com', 'santhosh.v@treebohotels.com']

    USERNAME = "AKIAYZ7GE5SNATIB23OR"
    PASSWORD = "AoC9Wwy92HhTiO3dFtF8B8PuvnA1zKTgf4JgXcwaab9i"

    # typical values for text_subtype are plain, html, xml
    text_subtype = 'plain'
    today_date = datetime.now().day
    month = datetime.now().strftime('%B')
    year = datetime.now().year

    body = "Please find below attached file for sme calling"
    subject = "SME Calling - " + str(today_date) + " " + month + " ," + str(year)
    try:
        msg = MIMEMultipart()
        # msg = MIMEText(content, text_subtype)
        msg['Subject'] = subject
        msg['From'] = sender  # some SMTP servers will do this automatically, not all

        msg.attach(MIMEText(body, 'plain'))
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open("sme.csv", "rb").read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="sme.csv"')

        msg.attach(part)
        conn = SMTP(SMTPserver)
        conn.set_debuglevel(False)
        conn.login(USERNAME, PASSWORD)
        try:
            conn.sendmail(sender, destination, msg.as_string())
        finally:
            conn.quit()
    except Exception, exc:
        sys.exit("mail failed; %s" % str(exc))
