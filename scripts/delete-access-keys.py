import datetime, boto3, csv, os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

IAM = boto3.client('iam')


def access_key(user):
    try:
        keydetails = IAM.list_access_keys(UserName=user)
        # Some user may have 2 access keys. So iterating over them and listing the details of active access key.
        for keys in keydetails['AccessKeyMetadata']:
            if keys['Status'] == 'Active' and (time_diff(keys['CreateDate'])) >= 90:
                print (keys['UserName'], keys['AccessKeyId'], time_diff(keys['CreateDate']), sep=',')
                delete_key = IAM.delete_access_key(
                     UserName=keys['UserName'],
                     AccessKeyId=keys['AccessKeyId']
                 )
                print(delete_key)
                
    except:
        print("Exception occured")


def time_diff(keycreatedtime):
    # Getting the current time in utc format
    now = datetime.datetime.now(datetime.timezone.utc)
    # Calculating diff between two datetime objects.
    diff = now - keycreatedtime
    # Returning the difference in days
    return diff.days


if __name__ == '__main__':
    """ In this main function, we are fetching the users from IAM and passing the username to access_key() """
    # This returns a dictionary response
    details = IAM.list_users(MaxItems=300)
    # Header information
    print ("Username", "AccessKey", "KeyAge", sep=',')
    for user in details['Users']:
        access_key(user['UserName'])
