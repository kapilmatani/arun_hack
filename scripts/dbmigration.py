import os
import sys
import boto3
import json
import psycopg2

#os.system('git clone ' + sys.argv[1])
#os.system('git clone https://punyam_agrawal@bitbucket.org/treebo/dbmigrate.git')
#os.system('cd /var/lib/jenkins/workspace/devops-p-dbmigration/service_repo && virtualenv -p python venv && source venv/bin/activate')
#os.system('sudo pip install click && sudo pip install psycopg2')
#os.system('cd /var/lib/jenkins/workspace/devops-p-dbmigration/dbmigrate && pip install .')
#os.system('cd /var/lib/jenkins/workspace/devops-p-dbmigration/service_repo)
session = boto3.session.Session()
if(sys.argv[2] == 'production'):
    client = session.client(
            service_name='secretsmanager',
            region_name='ap-south-1',
        )
if(sys.argv[2] == 'staging'):
    client = session.client(
            service_name='secretsmanager',
            region_name='ap-southeast-1',
        )
if(sys.argv[2] == 'africa'):
    client = session.client(
            service_name='secretsmanager',
            region_name='af-south-1',
        )

if(sys.argv[2] == 'production' or sys.argv[2] == 'staging'):
    get_secret_value_response = client.get_secret_value(
            SecretId=sys.argv[1]+"/"+sys.argv[2]+"/"+sys.argv[4]+"/apps/"+sys.argv[3]+"/postgres"
        )
    print("xyz",sys.argv[2])
    print(sys.argv[1]+"/"+sys.argv[2]+"/"+sys.argv[4]+"/apps/"+sys.argv[3]+"/postgres")
if(sys.argv[2] == 'africa'):
    get_secret_value_response = client.get_secret_value(
            SecretId=sys.argv[1]+"/production/"+sys.argv[4]+"/apps/"+sys.argv[3]+"/postgres"
        )
    print("abcd",sys.argv[2])
    print(sys.argv[1]+"/production/"+sys.argv[4]+"/apps/"+sys.argv[3]+"/postgres")
print(get_secret_value_response['SecretString'])
res = json.loads(get_secret_value_response['SecretString'])
##res1 = res.json()
print(res['username'])
print(res['password'])
print(res['host'])
print(res['port'])
print(res['dbname'])
#print(res['schema_name'])

if sys.argv[1] != 'treebo':
    text = '''[
        {
            "db_user": "'''+res['username']+'''",
            "db_password": "'''+res['password']+'''",
            "db_host": "'''+res['host']+'''",
            "db_port": "'''+res['port']+'''",
            "db_name": "'''+res['dbname']+'''",
            "db_schema": "'''+sys.argv[1]+'''_schema"
        }
    ]'''
else:
     text = '''[
        {
            "db_user": "'''+res['username']+'''",
            "db_password": "'''+res['password']+'''",
            "db_host": "'''+res['host']+'''",
            "db_port": "'''+res['port']+'''",
            "db_name": "'''+res['dbname']+'''",
            "db_schema": "public"
        }
    ]'''
if(sys.argv[2] == 'africa'):
    print('dbcred.json created')
    cmd = "echo '"+text+"'>> /var/lib/jenkins/workspace/devops-p-africa-dbmigration/service_repo/db_creds.json"
    os.system('rm -rf /var/lib/jenkins/workspace/devops-p-africa-dbmigration/service_repo/db_creds.json')
    os.system('touch /var/lib/jenkins/workspace/devops-p-africa-dbmigration/service_repo/db_creds.json')
    os.system(cmd)
if(sys.argv[2] == 'production'):
    print('dbcred.json created')
    cmd = "echo '"+text+"'>> /var/lib/jenkins/workspace/devops-p-dbmigration/service_repo/db_creds.json"
    os.system('rm -rf /var/lib/jenkins/workspace/devops-p-dbmigration/service_repo/db_creds.json')
    os.system('touch /var/lib/jenkins/workspace/devops-p-dbmigration/service_repo/db_creds.json')
    os.system(cmd)
if(sys.argv[2] == 'staging'):
    print('dbcred.json created')
    cmd = "echo '"+text+"'>> /var/lib/jenkins/workspace/devops-s-dbmigration/service_repo/db_creds.json"
    os.system('rm -rf /var/lib/jenkins/workspace/devops-s-dbmigration/service_repo/db_creds.json')
    os.system('touch /var/lib/jenkins/workspace/devops-s-dbmigration/service_repo/db_creds.json')
    os.system(cmd)

con = psycopg2.connect(database = res['dbname'], user = res['username'], host = res['host'], password = res['password'], port = res['port'])
cur = con.cursor()
cur.execute("SELECT EXISTS (SELECT FROM pg_tables WHERE  schemaname = '" +sys.argv[1]+"_schema' AND tablename  = 'migrations');")
output = cur.fetchall()
for row in output:
    print(row[0])
if row[0] == False:
    print('Creating migrations table in new schema')
    cur.execute('CREATE TABLE migrations (version varchar not null, applied_at timestamp with time zone not null, rollbacked boolean);')
con.commit()


#os.system('cd /var/lib/jenkins/workspace/devops-p-dbmigration/dbmigrate')
#os.system('dbmigrate upgrade')
#print(os.getcwd() + "\n")
#print("connected")