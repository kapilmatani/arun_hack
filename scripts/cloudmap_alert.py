import boto3
import time
import requests
import json

webhook_url = "https://hooks.slack.com/services/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"

client = boto3.client(service_name='servicediscovery',region_name='ap-south-1')
endtime=time.time()+900.0 #15minutes
count_crs = 0
count_payment = 0
count_reward = 0
count_taxrate = 0
while (time.time()<endtime):
    response_crs = client.discover_instances(
    NamespaceName='prod',
    ServiceName='crs-p-crsapi-authz')
    if(len(response_crs['Instances']) < 2):
        count_crs = count_crs+1

    response_payment = client.discover_instances(
    NamespaceName='prod',
    ServiceName='direct-p-payment')
    if(len(response_payment['Instances']) < 2):
        count_payment = count_payment+1
        
    response_reward = client.discover_instances(
    NamespaceName='prod',
    ServiceName='direct-p-reward')
    if(len(response_reward['Instances']) < 2):
        count_reward = count_reward+1
    
    response_taxrate = client.discover_instances(
    NamespaceName='prod',
    ServiceName='rms-p-shared-taxrackratepo')
    if(len(response_taxrate['Instances']) < 3):
        count_taxrate = count_taxrate+1
        
    time.sleep(300) #5minutes

if(count_crs>1):
    payload = str({"text": "Alert: Check cloudmap prod for service crs-p-crsapi-authz, instance count is " + len(response_crs['Instances'])})
    requests.post(webhook_url,payload)
    
if(count_payment>1):
    payload = str({"text": "Alert: Check cloudmap prod for service direct-p-payment, instance count is " + len(response_payment['Instances'])})
    requests.post(webhook_url,payload)

if(count_reward>1):
    payload = str({"text": "Alert: Check cloudmap prod for service direct-p-reward, instance count is " + len(response_reward['Instances'])})
    requests.post(webhook_url,payload)

if(count_taxrate>1):
    payload = str({"text": "Alert: Check cloudmap prod for service rms-p-shared-taxrackratepo, instance count is " + len(response_taxrate['Instances'])})
    requests.post(webhook_url,payload)
