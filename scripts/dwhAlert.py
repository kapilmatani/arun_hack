import requests
import boto3

url = 'https://hooks.slack.com/services/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'
def send_alert(url):
    payload_payment_success = str({"text": "DWH is still not available. Please check"})
    res1 = requests.post(url, payload_payment_success)
    return res1

try:
    client = boto3.client('redshift')
    response = client.describe_clusters(ClusterIdentifier='dwh')

    AvailabilityStatus=response['Clusters'][0]['ClusterAvailabilityStatus']
    if AvailabilityStatus != 'Available':
        res1=send_alert(url)
        print('Alert Sent')
except Exception as error:
    print('Exception thrown {}'.format(error))
