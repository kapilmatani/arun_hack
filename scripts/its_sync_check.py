import itertools
import csv
from datetime import datetime, timedelta
import psycopg2
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.base import MIMEBase
from email import encoders
import sys
from smtplib import SMTP_SSL as SMTP
import os

cs_id = []
diff_machine = []
diff_count = 0
cs_count = 0

def send_mail(subject,body,filename):
    SMTPserver = 'email-smtp.eu-west-1.amazonaws.com'
    sender = 'Inventory script <dev@treebohotels.com>'
    destination = ['punyam.agrawal@treebohotels.com','arun.midha@treebohotels.com']
    USERNAME = "AKIAYZ7GE5SNKX74N2C7"
    PASSWORD = "BDUL+OGzvSDqb0Cyne9Pm+vBZIN710G/g3QdATBsSopl"

    try:
        message = MIMEMultipart()
        message['Subject'] = subject
        message['From'] = sender
        message.attach(MIMEText(body,'plain'))

        if filename:
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(filename, "rb").read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename={}'.format(filename))
            message.attach(part)
            connection = SMTP(SMTPserver)
            connection.set_debuglevel(False)
            connection.login(USERNAME,PASSWORD)
            try:
                connection.sendmail(sender, destination, message.as_string())
            finally:
                connection.quit()

        else:
            connection = SMTP(SMTPserver)
            connection.set_debuglevel(False)
            connection.login(USERNAME, PASSWORD)
            try:
                connection.sendmail(sender, destination, message.as_string())
            finally:
                connection.quit()
        print("Mail Sent:please check")
    except Exception as error:
        sys.exit("mail failed; {}".format(error))


con3 = psycopg2.connect(database = 'cataloging_service', user = 'cataloging', port = '5432', host = 'cataloging-service-rds-01.treebo.com', password = '9jU3UbwmfAHHZKLEFT55')
cur4 = con3.cursor()
cur4.execute("select id from property where status = 'LIVE' and name ilike 'Treebo%';")
output4 = cur4.fetchall()
for row4 in output4:
    cs_id.append(row4[0])
    cs_count = cs_count + 1
print(cs_count)


con = psycopg2.connect(database = 'crs_db', user = 'crs_user', port = '5432', host = 'crs-rds-writer.treebo.com', password = 'crs_paSSword')
cur_inv = con.cursor()
cur_dnr = con.cursor()
cur_room = con.cursor()
con1 = psycopg2.connect(
        database = 'inventory_throttling',
        user = 'optimus_write',
        port = '6432',
        host = 'pgbouncer-cluster.treebo.com'
        )
cur_its = con1.cursor()
curr_date = datetime.today().strftime('%Y-%m-%d')
#print(curr_date)
tomm_date = datetime.today()+timedelta(days=1)
tomm_date = tomm_date.strftime('%Y-%m-%d')
next_date = datetime.today()+timedelta(days=2)
next_date = next_date.strftime('%Y-%m-%d')
#print(tomm_date)
file = open('tmp_file.txt', "w")
body = "ITS not in sync with CRS for following properties attached: testing \n"
all_dates = [curr_date,tomm_date,next_date]
for date in all_dates:
    print(date)
    for csid in cs_id:
        crs_rtype = []
        crs_count = []
        crs_date = []
        its_rtype = []
        its_count = []
        its_date = []
        count_crs = 0
        count_its = 0
        query = "select room_type_id, count, date from room_type_inventory_availability where hotel_id = '" + csid + "' and date = '" + date + "' order by room_type_id;"
        cur_inv.execute(query)
        output = cur_inv.fetchall()
        for row in output:
            crs_rtype.append(row[0])
            crs_count.append(row[1])
            crs_date.append(row[2])
            count_crs = count_crs + 1
        cur_its.execute("select room_code, availablerooms, date  from its_availability as ia, its_room as ir, its_hotel as ih where ih.catalog_id = '" + csid + "' and ia.room_id = ir.id and ih.id = ir.hotel_id and date = '" + date + "'order by room_code;")
        output1 = cur_its.fetchall()
        for row1 in output1:
            its_rtype.append(row1[0])
            its_count.append(row1[1])
            its_date.append(row1[2])
            count_its = count_its + 1
        final_count = 0
        dnr_count = 0
        if(count_its == count_crs):
            for i in range(count_its):
                if(crs_rtype[i]==its_rtype[i] and crs_count[i]==its_count[i]):
                    final_count = final_count+1
            if(final_count == count_crs):
                print("matched for hotel: " + csid)
                
        else:
            for j in range(count_crs):
                cur_dnr.execute("select count(*) from dnr where hotel_id = '" + csid + "' and start_date <= '" + date + "' and end_date >= '" + date + "' and status = 'active' and room_id in ( select room_id from room where hotel_id = '" + csid + "' and room_type_id = '" + crs_rtype[j] + "');")
                output2 = cur_dnr.fetchall()
                cur_room.execute("select count(*) from room where hotel_id = '" + csid + "' and room_type_id = '" + crs_rtype[j] + "';")
                output3 = cur_room.fetchall()
                for row2,row3 in zip(output2,output3):
                    if(row2[0] == row3[0]):
                        dnr_count = dnr_count + 1

            if(dnr_count == count_crs):
                print("matched for hotel: " + csid)
            else:
                file.write('Please check: for following properties, inventory of ITS is not in sync with CRS  \n\n')
                body = body + "::Catalog ID :" + csid + " for date :" + date + "\n"
                print("not matched for hotel: " + csid)
                diff_machine.append(csid)
                diff_count = diff_count + 1

if(diff_count>0):
    subject="ITS sync issue with crs"
    #send_mail(subject,body,'tmp_file.txt')
print(diff_count)

file.close()
