import requests
import sys
import json


t = requests.get('http://ci.treebo.com:8080/computer/api/json?pretty=true',auth = ('jenkins.user', 'N;Y[^bP7.'))
webhook = 'https://hooks.slack.com/services/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'

resp = t.json()

for comp in resp['computer']:
    status = (str(comp['offline']))
    for name in comp["assignedLabels"]:
        if(name['name'] != "devops-p-jenkins-slave-docker-grid-01b" and status == 'True'):
            print(name['name'])
            payload = str({"text": "please check, jenkins node: " + name['name'] + " is down"})
            requests.post(webhook,payload)