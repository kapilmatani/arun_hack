import psycopg2
from datetime import datetime,date
from datetime import timedelta
from configparser import ConfigParser
import sys
import os
class database:
    def __init__(self, db_name,db_user,end_point,password):
        self.name = db_name
        self.user = db_user
        self.end_point = end_point
        self.password = password
        self.port = '5432'
class db_table:
    def __init__(self,name,time):
        self.table_name = name
        self.months = time
config = ConfigParser()
config.read('configuration.ini')
# create database object
name = config.get('Database','name')
end_point = config.get('Database','end_point')
db_user = config.get('Database','user')
password = config.get('Database','password')
db = database(name,db_user,end_point,password)
# create queue of table object
tables = []
number_of_tables = int(sys.argv[1])
for i in range(1,number_of_tables+1):
    tab_name = config.get('DB_table_'+str(i),'name')
    months = config.get('DB_table_'+str(i),'time')
    custom_table = db_table(tab_name,months)
    tables.append(custom_table)
print(len(tables))
def find_number_of_records(tables,cur):
    now = datetime.now()
    day_before_yesterday = now - timedelta(days=90)
    day_before_yesterday="'" + str(day_before_yesterday) + "'"
    count = []
    for table in tables:
        psql_select_query = 'SELECT COUNT(*) FROM {} WHERE modified_at <'.format(table.table_name) + day_before_yesterday
        cur.execute(psql_select_query)
        result = cur.fetchall()
        count.append([table.table_name,result[0][0]])
    return count
def delete_old_records(tables,cur):
    try:
        now = datetime.now()
        day_before_yesterday = now - timedelta(days=90)
        day_before_yesterday="'" + str(day_before_yesterday) + "'"
        print('Deleting the records')
        for table in tables:
            psql_delete_query = '''DELETE FROM {} WHERE id = '692' '''.format(table.table_name)
            cur.execute(psql_delete_query)
        print('records deleted')
    except Exception as e:
        print('error is {} not found'.format(e))
def compress_tables(tables,database):
    try:
        result = []
        for table in tables:
            print('compressing {}'.format(table.table_name))
            folder_name = str(date.today()) + '-' + database.name
            dump_command = 'pg_dump -Fc -v -h {} -p 5432 -U {} -d {} --format=c -w > {}.tar'.format(database.end_point,database.user,database.name,folder_name)
            r = os.popen(dump_command)
            result.append(r.read())
            gzip_command = 'gzip ' + folder_name + '.tar'
            g = os.popen(gzip_command)
            print('compressed {}'.format(table.table_name))
        return result
    except Exception as e:
        print('error is {} not found'.format(e))

try:
    connection = psycopg2.connect(host=db.end_point, user=db.user, dbname=db.name, port=db.port, password=db.password)
    cur = connection.cursor()
    ret = tables
    #num = find_number_of_records(ret,cur)
    #for i in range(len(ret)):
    #    print(ret[i].table_name)
    #for i in range(len(num)):
    #    print(num[i])
    print('++++++++++++++++++++++++++++++++++++++++++++++++++')
    compress_tables(ret,db)
    #delete_old_records(ret, cur)
    connection.commit()

except (Exception, psycopg2.Error) as error:
    print(error)

finally:
    if(connection):
        cur.close()
        connection.close()
        print('closed')