from boto3 import client
import boto3
import datetime
week_ago = datetime.datetime.today() - datetime.timedelta(days=7)
conn = client('s3')
client = boto3.client('s3')
for key in conn.list_objects(Bucket='cybertron-s-vortex')['Contents']:
    if(key['Key'].find('e-reg-cards') == 0 and key['LastModified']):
        if((key['LastModified'].date()-week_ago.date()).days <= 0):
            print(key['Key'])
            print(key['LastModified'].date())
            response = client.delete_object(
            Bucket='cybertron-s-vortex',
            Key=key['Key']
            )