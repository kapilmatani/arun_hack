import csv
import os
import sys
from smtplib import SMTP_SSL as SMTP
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders

def send_mail_gmail(attachment_path_list=None):

    try:
        SMTPserver = 'email-smtp.eu-west-1.amazonaws.com'
        EMAIL_PORT = 587
        sender = 'Staging Scripts <dev@treebohotels.com>'
        destination = ['parth.agarwal@treebohotels.com','arun.midha@treebohotels.com']
        username = "AKIAYZ7GE5SNKX74N2C7"
        password = "BDUL+OGzvSDqb0Cyne9Pm+vBZIN710G/g3QdATBsSopl"

        msg_text = 'Please find attached the repos with hardcoded database end-points'
        subject = "Output for the repo search job"

        s = SMTP(SMTPserver)
        s.login(username, password)
        #s.set_debuglevel(1)
        msg = MIMEMultipart()
        recipients = destination
        msg['Subject'] = subject
        if sender is not None:
            msg['From'] = sender
        msg['To'] = ", ".join(recipients)
        if attachment_path_list is not None:
            for each_file_path in attachment_path_list:
                try:
                    file_name=each_file_path.split("/")[-1]
                    part = MIMEBase('application', "octet-stream")
                    part.set_payload(open(each_file_path, "rb").read())

                    encoders.encode_base64(part)
                    part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                    msg.attach(part)
                except:
                    print("could not attache file")
        msg.attach(MIMEText(msg_text,'plain'))
        s.sendmail(sender, recipients, msg.as_string())
    except Exception as e:
        print("Exception occoured in Email function : {}".format(e))
    finally:
        s.quit()
try:
    filename = sys.argv[1]
    output_files = []
    with open(filename) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            git_clone_url,repo_name = row[0],row[1]
            git_clone_command = ''' git clone {} '''.format(git_clone_url)
            current_dir = os.getcwd()
            git_grep_command = ''' git grep "rds.amazonaws.com" `git show-ref --heads` > {}/{}-out.txt '''.format(current_dir,repo_name)

            print(git_clone_command)
            os.system(git_clone_command)

            print('cd {}'.format(repo_name))
            os.system('cd {} && {}'.format(repo_name,git_grep_command))
            print(git_grep_command)

            print("Git-Search done for {}. Output is in file {}/{}-out.txt".format(repo_name,current_dir,repo_name))
            print('Removing the cloned repo h...')
            os.system('rm -rf {}'.format(repo_name))
            output_files.append('{}-out.txt'.format(repo_name))
except Exception as e:
    print("Exception occoured in Cloning process block : {}".format(e))

try:
    attachments = []
    for file in output_files:
        filesize = os.path.getsize(file)
        if filesize > 0:
            attachments.append(file)
    if attachments==[]:
        attachments = None
    send_mail_gmail(attachments)
    print('mail-sent')
except Exception as e:
    print(e)

