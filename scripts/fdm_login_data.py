import psycopg2
import csv ,sys ,os
from os import path
from datetime import date
from smtplib import SMTP_SSL as SMTP
from email import encoders
from email.mime.text import MIMEText
import smtplib
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart

def send_mail(subject,body,filename=None):
    SMTPserver = 'email-smtp.eu-west-1.amazonaws.com'
    sender = 'Staging Scripts <dev@treebohotels.com>'
    destination = ['shri.prasanna@treebohotels.com']
    USERNAME = "AKIAYZ7GE5SNKX74N2C7"
    PASSWORD = "BDUL+OGzvSDqb0Cyne9Pm+vBZIN710G/g3QdATBsSopl"

    try:
        message = MIMEMultipart()
        message['Subject'] = subject
        message['From'] = sender
        message.attach(MIMEText(body,'plain'))

        if filename:
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(filename, "rb").read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename={}'.format(filename))
            message.attach(part)
            connection = SMTP(SMTPserver)
            connection.set_debuglevel(False)
            connection.login(USERNAME,PASSWORD)
            try:
                connection.sendmail(sender, destination, message.as_string())
            finally:
                connection.quit()

        else:
            connection = SMTP(SMTPserver)
            connection.set_debuglevel(False)
            connection.login(USERNAME, PASSWORD)
            try:
                connection.sendmail(sender, destination, message.as_string())
            finally:
                connection.quit()
        print("Mail Sent:please check")
    except Exception as error:
        sys.exit("mail failed; {}".format(error))



# Auth_N Credentials
authN_host = 'direct-p-growth-slave.treebo.com'
authN_db = 'treebo_auth'
authN_user = 'growth'
authN_password = 'XYgs1CnY3rw0'
port = 5432

# Auth_Z Credentials
authZ_host = 'web-slave-rds.treebo.com'
authZ_db = 'authz'
authZ_user = 'authz_admin'
authZ_password= 'K3D5#^sdi534'

# Take Data form Auth_N db
fdm_users = {}
try:
    authN_connection = psycopg2.connect(host=authN_host, user=authN_user, dbname=authN_db, port=port, password=authN_password)
    authN_cur = authN_connection.cursor()
    authN_select_query = ''' SELECT id,email,first_name FROM user_user WHERE last_name='FDM' AND last_login > (NOW() - INTERVAL '3 days') AND email IS NOT NULL ORDER BY id '''
    authN_cur.execute(authN_select_query)
    result = authN_cur.fetchall()
    if result:
        for tup in result:
            fdm_users[tup[0]] = [tup[1],tup[2]]
except (Exception, psycopg2.Error) as error:
    body="FDM_User script failed in Auth_N block due to : {}".format(error)
    subject="FDM Script failure"
    send_mail(subject,body)
finally:
    if(authN_connection):
        authN_cur.close()
        authN_connection.close()
        print('closed from Auth_N DB')

# Take Data form Auth_Z db
try:
    ids,resources = '',[]
    for k in fdm_users.keys():
       ids += "'" + str(k) + "'" + ","
    ids = ids[:-1]

    authZ_connection = psycopg2.connect(host=authZ_host, user=authZ_user, dbname=authZ_db, port=port, password=authZ_password)
    authZ_cur = authZ_connection.cursor()
    authZ_select_query = ''' SELECT user_id,resource_id FROM user_role WHERE user_id IN ({}) ORDER BY user_id '''.format(ids)
    authZ_cur.execute(authZ_select_query)
    result = authZ_cur.fetchall()
    if result:
        for tup in result:
            resources.append([tup[0],tup[1]])
except (Exception, psycopg2.Error) as error:
    body="FDM_User script failed in Auth_Z block due to : {}".format(error)
    subject="FDM Script failure"
    send_mail(subject,body)

finally:
    if(authZ_connection):
        authZ_cur.close()
        authZ_connection.close()
        print('closed from Auth_Z DB')

# Some pre-processing and handling
for resource in resources:
    if resource[1] is not None:
        fdm_users[resource[0]].append(resource[1])
    else:
        fdm_users[resource[0]].append('NaN')

# Creating the CSV FILE
try:
    filename = 'fdm_data' +  str(date.today()) + '.csv'
    fields = ['User_ID','Email','Name','Hotel_id']
    with open(filename,'w',newline='') as csvFile:
        csv_writer = csv.DictWriter(csvFile,fieldnames=fields)
        csv_writer.writeheader()

        for k,v in fdm_users.items():
            if len(v)<3:
                csv_writer.writerow({'User_ID': k, 'Email': v[0], 'Name': v[1], 'Hotel_id': 'None'})
            else:
                csv_writer.writerow({'User_ID':k,'Email':v[0],'Name':v[1],'Hotel_id':v[2]})
    body="PFA FDM Data for today"
    subject="FDM Script Success"
    send_mail(subject,body,filename)

except (Exception, psycopg2.Error) as error:
    #send email
    body="FDM_User script failed in CSV_FILE block due to : {}".format(error)
    subject="FDM Script failure"
    send_mail(subject,body)
finally:
    csvFile.close()

if path.exists(filename):
    os.system("rm -rf *.csv")
    print("removed all CSV files")
