import psycopg2,psycopg2.extensions
import requests

def delete_records_from_django_session(cur,connection):
    postgres_select_query = ''' select count(*) from django_session '''
    postgres_delete_query = '''DELETE FROM django_session'''

    cur.execute(postgres_select_query)
    number_of_records = cur.fetchall()[0][0]

    cur.execute(postgres_delete_query)
    connection.commit()
    print('Deleted {} records from django_session table'.format(number_of_records))

def delete_records_from_backup_django_session(cur,connection):
    postgres_select_query = ''' select count(*) from backup_django_session '''
    postgres_delete_query = '''DELETE FROM backup_django_session '''

    cur.execute(postgres_select_query)
    number_of_records = cur.fetchall()[0][0]

    cur.execute(postgres_delete_query)
    connection.commit()
    print('Deleted {} records from backup_django_session table'.format(number_of_records))


host_name='sharedapps2-rds.treebo.be'
db_name='treebo_auth'
db_user='crs_user'
db_pass='qtR8T456*wiopj'
dp_port='5432'
try:
    connection = psycopg2.connect(host=host_name, user=db_user, dbname=db_name, port=dp_port, password=db_pass)
    cur = connection.cursor()
    print('Connected to treebo_auth')
    delete_records_from_django_session(cur,connection)
    delete_records_from_backup_django_session(cur,connection)

    connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cur.execute(''' VACUUM ANALYZE django_session ''')
    cur.execute(''' VACUUM ANALYZE backup_django_session ''')
    print('ran vacuum')

except Exception as e:
    print(e)
    url = 'https://hooks.slack.com/services/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'
    payload = str({"text": "auto_vacuum_treeboauthStage script on Jenkins failed with error : {}. Please check".format(e)})
    result = requests.post(url, payload)
    
finally:
    if connection:
        connection.close()
        cur.close()
        print('closed from treebo_auth')