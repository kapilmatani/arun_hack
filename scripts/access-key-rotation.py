import datetime, boto3, csv, os, json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

IAM = boto3.client('iam')


def access_key(user):
    try:
        keydetails = IAM.list_access_keys(UserName=user)
        print(keydetails)
        tagdetails = IAM.list_user_tags(UserName=user)
        print(tagdetails)
        tag_name = tagdetails['Tags']
        print(tag_name)
        # Some user may have 2 access keys. So iterating over them and listing the details of active access key.
        for keys in keydetails['AccessKeyMetadata']:
            if keys['Status'] == 'Active' and (time_diff(keys['CreateDate'])) >= 84:
                print (keys['UserName'], keys['AccessKeyId'], time_diff(keys['CreateDate']), sep=',')
                # delete_key = IAM.delete_access_key(
                #      UserName=keys['UserName'],
                #      AccessKeyId=keys['AccessKeyId']
                #  )
                # print(delete_key)
                create_key = IAM.create_access_key(UserName=keys['UserName'])
                print(create_key)
                new_access = create_key['AccessKey']
                print(new_access)
                new_access_key = new_access['AccessKeyId']
                print (new_access_key)
                new_secret_key = new_access['SecretAccessKey']
                print(new_secret_key)
                write_row = str(keys['UserName']) + "," + str(keys['AccessKeyId']) + "," +str(new_access_key) + "," + str(new_secret_key) + "\n\n"
                print(write_row)
                for tags in tag_name:
                    if tags['Key'] == 'is_human' and tags['Value'] == 'yes':
                        download_dir_user = "/tmp/new-access-keys-user.csv"
                        print(download_dir_user)
                        file_user = open(download_dir_user, "w")
                        row = "Username" + "," + "OldAccessid" + "," + "NewAccessId" + "," + "Secretkey" + "\n\n"
                        file_user.write(row)
                        file_user.write(write_row)
                        file_user.close()
                        receiver_user = user + '@treebohotels.com'
                        print(receiver_user)
                        receiver = [receiver_user]
                        msg = MIMEMultipart()
                        msg['From'] = "ci@treebohotels.com"
                        msg['To'] = ", ".join(receiver)
                        msg['Subject'] = 'Updated AWS Access Keys for your IAM user'

                        # email content
                        message = """PFA the new Access keys for your AWS user and kindly update them as the old ones have been deleted due to rotation
                                """
                        print(message)
                        msg.attach(MIMEText(message, 'html'))
                        attachment = open(download_dir_user, 'rb')
                        file_name = os.path.basename(download_dir_user)
                        part = MIMEBase('application', 'octet-stream')
                        part.set_payload(attachment.read())
                        part.add_header('Content-Disposition',
                                        'attachment',
                                        filename=file_name)
                        encoders.encode_base64(part)
                        msg.attach(part)

                        smtpserver = smtplib.SMTP('smtp.gmail.com', 587)
                        smtpserver.starttls()
                        smtpserver.login('ci@treebohotels.com', 'an4@#sa1')
                        smtpserver.sendmail('ci@treebohotels.com', receiver, msg.as_string())
                        smtpserver.quit()

                    else:
                        file1.write(write_row)


    except Exception as e:
        print("Exception occured {}",e)


def time_diff(keycreatedtime):
    # Getting the current time in utc format
    now = datetime.datetime.now(datetime.timezone.utc)
    # Calculating diff between two datetime objects.
    diff = now - keycreatedtime
    # Returning the difference in days
    return diff.days
#
def send_mail(files):

    receiver = ['aishwary.varshney@treebohotels.com','arun.midha@treebohotels.com','mayank.khandelwal@treebohotels.com',
-                'ashok.kumar@treebohotels.com','kapil.matani@treebohotels.com','rohit.jain@treebohotels.com','prashant.kumar@treebohotels.com']
    msg = MIMEMultipart()
    msg['From'] = "ci@treebohotels.com"
    msg['To'] = ", ".join(receiver)
    msg['Subject'] = 'Updated AWS Access Keys'

    # email content
    message = """PFA the new Access keys for the users and kindly update them as the old ones have been deleted due to rotation
        """

    msg.attach(MIMEText(message, 'html'))

    attachment = open(files, 'rb')
    file_name = os.path.basename(files)
    part = MIMEBase('application', 'octet-stream')
    part.set_payload(attachment.read())
    part.add_header('Content-Disposition',
                    'attachment',
                    filename=file_name)
    encoders.encode_base64(part)
    msg.attach(part)

    # sends email

    smtpserver = smtplib.SMTP('smtp.gmail.com', 587)
    smtpserver.starttls()
    smtpserver.login('ci@treebohotels.com', 'an4@#sa1')
    smtpserver.sendmail('ci@treebohotels.com', receiver, msg.as_string())
    smtpserver.quit()


if __name__ == '__main__':
    # """ In this main function, we are fetching the users from IAM and passing the username to access_key() """
    download_dir = "/tmp/new-access-keys.csv"
    file1 = open(download_dir, "w")
    row = "Username" + "," + "OldAccessid" + "," +  "NewAccessId" + "," +"Secretkey" + "\n\n"
    file1.write(row)
    # This returns a dictionary response
    details = IAM.list_users(MaxItems=300)
    # Header information
    print ("Username", "AccessKey", "KeyAge", sep=',')
    for user in details['Users']:
        access_key(user['UserName'])
    file1.close()
    count = 0
    with open('/tmp/new-access-keys.csv', 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            print(row)
            count = count + 1
    print(count)
    if count != 2 and count != 0:
        send_mail(download_dir)

