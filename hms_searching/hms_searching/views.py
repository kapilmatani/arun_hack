from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from . import db
import psycopg2


hostname = 'rds-replica-hmssync.cadmhukcksta.ap-southeast-1.rds.amazonaws.com'
username = 'hmssync'
password = 'arHunNuj5'
database = 'hmssync'


@api_view(['GET', 'POST'])
def create_post(request):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        return Response({"hello"})

    elif request.method == 'POST':
        query = request.POST.get("address")
        myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
        data = doQuery(myConnection,query)
        myConnection.close()
        return Response({"data": data})

# Simple routine to run a query on a database and print the results:
def doQuery( conn, query) :
   cur = conn.cursor()

   cur.execute( "SELECT * from bookingstash_reservationbooking where wrid =" + "'" + query.strip() + "'"  )

   fetchvar= cur.fetchall()

   print fetchvar
   cnt =  len(fetchvar)
   
   if (cnt<1):
	cur.execute( "SELECT * from bookingstash_reservationbooking where booking_id  =" + "'" + query.strip() + "'"  )
	fetchvar= cur.fetchall()
   return fetchvar


def doMultipleQuery( conn, first_name, last_name, email, phone) :
    try:
        cur = conn.cursor()
	
        query_string = (
        "SELECT booking_id,group_code,guest_email,guest_phone,guest_first_name,guest_last_name,checkin_date,checkout_date,hotel_code,adult_count,status,source,stay_source,group_stay_source,bia_status,child_count from bookingstash_reservationbooking where guest_first_name ilike " + "'%" + first_name + "%'" + "and guest_last_name ilike " + "'%" + last_name + "%'" + "and guest_phone ilike " + "'%" + phone + "%'" + "and guest_email ilike " + "'%" + email + "%'")
        cur.execute(query_string)
	fetchvar= cur.fetchall()
        return fetchvar

    except Exception as E:
        print E

@api_view(['GET', 'POST'])
def multiple_post(request):
    try:

        if request.method == 'GET':
            return Response({"hello"})

        elif request.method == 'POST':

            first_name = request.POST.get("first_name")

            last_name = request.POST.get("last_name")
            email = request.POST.get("email")
            phone = request.POST.get("phone")

            myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
            print "connection success"
            data = doMultipleQuery(myConnection,first_name, last_name, email, phone )
            myConnection.close()
            print data
            return Response({"data": data})
    except Exception as E:
        print E
